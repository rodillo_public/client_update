''' tk_image_slideshow3.py
create a Tkinter image repeating slide show
tested with Python27/33  by  vegaseat  03dec2013
'''
from PIL import Image, ImageTk, ExifTags
import io
import sys
import requests
import time
import json
import os
import subprocess
import socket
from threading import Thread
from itertools import cycle
try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk
class App(tk.Tk):
    def __init__(self, image_files, x, y, delay):
        tk.Tk.__init__(self)
        self.attributes("-fullscreen", True)
        self.configure(background='black')

        self.delay = delay

        self.image_files = image_files
        self.picture_display = tk.Label(self, borderwidth=0)
        logfilename = '/data/rodillo/menu.log'
        with open(logfilename, "a+") as logfile:
            subprocess.Popen(['/etc/rodillo/rodillo_menu'], stdout=logfile)
        self.picture_display.pack()

        self.bind('<Escape>', self.quit_prog)
        self.bind('<Button-1>', self.save_button_pressed_time)
        self.bind('<ButtonRelease-1>', self.start_menu)
    def show_slides(self):
        global counter
        if counter>=2:
            t = Thread(target=self.update_list, args=([counter]))
            t.start()
        else:
            counter=counter+1
        global current
        img_object = self.photo_image(self.image_files[current])

        self.picture_display.config(image=img_object)
        self.picture_display.image = img_object

        # shows the image filename, but could be expanded
        # to show an associated description of the image
        global current_after_task
        if not current_after_task == 0:
            self.after_cancel(current_after_task)
        current_after_task = self.after(self.delay, self.show_slides)
        print('current vor Erhoehung ' + str(current))
        current = current + 1
        if current >= len(self.image_files):
            current = 0
        print('current nach Erhoehung ' + str(current))
    def run(self):
        self.mainloop()

    def photo_image(self, jpg_filename):
        print(jpg_filename)
        with io.open(jpg_filename, 'rb') as ifh:
            pil_image = Image.open(ifh)
            for orientation in ExifTags.TAGS.keys() : 
                if ExifTags.TAGS[orientation]=='Orientation' : break 
            try :
                exif=dict(pil_image._getexif().items())
                if exif[orientation] == 3 : 
                    pil_image=pil_image.rotate(180, expand=True)
                elif exif[orientation] == 6 : 
                    pil_image=pil_image.rotate(270, expand=True)
                elif exif[orientation] == 8 : 
                    pil_image=pil_image.rotate(90, expand=True)
            except:
                pass
            


            pil_image.thumbnail((self.winfo_screenwidth(), self.winfo_screenheight()), Image.ANTIALIAS)
            return ImageTk.PhotoImage(pil_image)

    def quit_prog(self, event):
        sys.exit()

    def save_button_pressed_time(self, event):
        global press_time
        press_time = time.time()

    def start_menu(self, event):
        global press_time
        time_now = time.time()
        time_diff = time_now - press_time
        print('diff ' + str(time_diff))
        if time_diff >= 5:
            UDP_IP = "127.0.0.1"
            UDP_PORT = 9234
            MESSAGE = "open"
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
        else:
            
            global current
            current = current + 1
            if current >= len(self.image_files):
                current = 0
            print('current ' + str(current))

            self.show_slides()

    def update_list(self,i):
        global updateinprogress
        if updateinprogress == 1:
            print("update running. exiting...")
            return
        else:
            updateinprogress = 1
        try:
        	####################################################### VERSION ###############################################
            rodversion = 13
        	###############################################################################################################
            print("Update-Thread started...")
            rodillopath='/data/rodillo/'
            resp = requests.get('https://rodillo.bytewish.de/list_pictures.php?rodversion=' + str(rodversion), cert=(rodillopath + 'client.pem', rodillopath + 'client.key'))
            new_imagelist = []
            #print(resp.status_code)
            if resp.status_code == 200:
                global current
                jlist = json.loads(resp.text)
                for knocke in jlist["images"]:
                    tstring = rodillopath + "cache/" + str(knocke) + ".jpg"
                    if not os.path.isfile(tstring):
                        print(tstring + " is not there. Downloading...")
                        dlurl = "https://rodillo.bytewish.de/get_picture.php?imageId=" + str(knocke)
                        print(dlurl)
                        
                        current = 0
                        print('current ' + str(current))
                        r = requests.get(dlurl, cert=(rodillopath + 'client.pem', rodillopath + 'client.key'), stream=True)
                        print(r.status_code)
                        if r.status_code == 200:
                            with open(tstring, 'wb') as f:
                                for chunk in r.iter_content(1024):
                                    f.write(chunk)
                    new_imagelist.append(tstring)
                self.image_files = new_imagelist
                # Alle Bilder loeschen, die nicht in der Liste stehen
                cache_files = os.listdir(rodillopath + 'cache')
                for cache_file in cache_files:
                    tWithoutExtension = os.path.splitext(cache_file)[0]
                    if not any(tWithoutExtension in s for s in new_imagelist) and not cache_file == 'dummy.jpg':
                        # Bild taucht nicht in der Liste auf, loeschen
                        os.remove(rodillopath + 'cache/' + cache_file)
                if current >= len(self.image_files):
                    current = 0
                # Bildverzoegerung setzen
                if jlist["settings"]["delay"] >= 1000:
                    self.delay = jlist["settings"]["delay"]
            global counter
            counter = 0
            #global updateinprogress
            updateinprogress = 0
        except AttributeError as error:
            print("Attribute error:", error)
        except Exception:
            #global updateinprogress
            updateinprogress = 0
            print("Unexpected error:", sys.exc_info()[0])
            pass

current = 0
counter = 0
updateinprogress = 0
press_time = time.time()
current_after_task = 0

delay = 10000
rodillopath='/data/rodillo/'
image_files = []
cache_files = os.listdir(rodillopath + 'cache')
for cache_file in cache_files:
        image_files.append(rodillopath + 'cache/' + cache_file)

# upper left corner coordinates of app window
x = 100
y = 50
app = App(image_files, x, y, delay)

app.show_slides()
app.run()
